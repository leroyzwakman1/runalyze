#!/bin/bash

# This was an example taken from https://github.com/Runalyze/personal-api

if [ -e .runalyze.token ]; then
	token=`cat .runalyze.token`
else
	if [ -z $1 ]; then
		echo "usage $0 <token>"
		exit 1
	else
		token=$1
	fi
fi

shopt -s nullglob
shopt -s nocaseglob

while read file ; do
    echo "Parsing file '${file}' "
    datefull=`grep -Pio '<id>[0-9\-]+T[0-9\:]+Z</id>' "${file}"`
    datum=`echo ${datefull} | grep -Po '....-..-..'`
    tijd=`echo ${datefull} | grep -Po '..:..:..'`
    sport=`grep -Pio 'Sport="[a-z]+"' "${file}" | cut -f2 -d'=' | tr -d "\""`
    Duration=`grep -Pio 'TotalTimeSeconds>[0-9]+' "${file}" | grep -Po '[0-9\.]+' | awk '{s+=$1} END {print s}'`
    echo "Duration: ${Duration}"
    TotalTime=`python -c "import datetime; print(str(datetime.timedelta(seconds=${Duration})))"`
    echo "TotalTime ${TotalTime}"
    Distance=`grep -Pio 'DistanceMeters>[0-9\.]+' "${file}" | grep -Po '[0-9\.]+' | tail -1`
    echo "Distance ${Distance}"
    TotalKM=`python -c "print(round(${Distance}/1000, 2))"`
    echo "TotalKM ${TotalKM}"
    AVG=`python -c "print(round(${Distance}/${Duration}*3.6, 2))"`
    echo "AVG : ${AVG}"
    #MaxSpeedSeconds=`grep -Pio 'TotalTimeSeconds>[0-9]+"' "${file}" | cut -f2 -d'=' | tr -d "\""`
    activity=`basename "${file}" | cut -f1 -d'_'`
    echo "sport: ${sport}, activity: ${activity}"
    suggest=""
    case ${activity} in
        "Freestyle")
	    suggest="Walking"
	    ;;
        "Running")
            suggest="Running"
            ;;
        "Velo")
            suggest="Fietstrainer"
            ;;
        "Treadmill")
            suggest="Crosstrainer"
            ;;
        "Cycling")
            #Cycling can be Mountainbike, Transport, or Sport
	    suggest="Fietsen"
	    # > 25 km/h? --> Racefiets
	    avg_int=`echo ${AVG} | cut -f1 -d'.'`
	    if [ ${avg_int} -gt 25 ]; then
		    suggest="Wielrennen"
        fi
            ;;
        "Gym")
            suggest="Fitness"
            ;;
        *)
            suggest="NotImplementedYet"
            ;;
     esac

     #echo "time: ${Duration} - Distance ${Distance}"
     #echo "time: ${TotalTime} - Distance ${TotalKM}, AVG: ${AVG} km/h"
     echo "  $suggest"
             uploadsport=$(whiptail --clear --title "Choose Sport for ${activity}" --menu "\n Datum: ${datum} ${tijd} - ${TotalTime} uur, ${TotalKM} km, ${AVG} km/h" --default-item "$suggest" 28 78 0 \
                 "skip" "Skip this Item" \
                 "Fietsen" "Fiets als Transport" \
		 "Wielrennen" "Racefietsen"\
                 "Mountainbike" "Mountainbiken" \
                 "Fietstrainer" "Racefiets op Tacx" \
                 "Walking" "Wandelen" \
                 "Running" "Hardlopen" \
                 "Fitness" "Fitness" \
                 "Crosstrainer" "Crosstrainer" \
                 "Overige" "Overige" \
                 "Mountainbike" "Mountainbiken"  3>&1 1>&2 2>&3)

    if [[ ! $uploadsport == "skip" ]]; then
         echo "vervangen van ${sport} door ${uploadsport}"
	 sed -i -e "s/${sport}/${uploadsport}/g" "${file}"
         OUTPUT=$(curl -s --location --request POST 'https://runalyze.com/api/v1/activities/uploads' --header 'token: '${token} --form 'file=@'"${file}")
	 echo ${OUTPUT}
         if [[ $OUTPUT == *"No valid token"* ]]; then
             echo "token not valid"
             shopt -u nocaseglob
             shopt -u nullglob
             exit;
         fi
	 mv "${file}" "${file}.old"
    else
	    echo "skipping"
    fi


echo

done < <(find ~/TomTom\ Sports/ -name "*.tcx" -mtime -1)
#done < <(find ~/TomTom\ Sports/ -name "*.tcx" | head -4)

shopt -u nocaseglob
shopt -u nullglob
