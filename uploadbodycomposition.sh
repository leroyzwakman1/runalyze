#!/bin/bash

# This was an example taken from https://github.com/Runalyze/personal-api, adjusted for upload of endomondo weights, these came from the export; and has been changed to a single line

if [ -e .runalyze.token ]; then
	token=`cat .runalyze.token`
else 
	if [ -z $1 ]; then
		echo "usage $0 <token>"
		exit 1
	else
		token=$1
	fi
fi

while read line; do
	date=`echo $line | cut -f1 -d ';'`
	time=`echo $line | cut -f2 -d';'`
	weight=`echo $line | cut -f3 -d';'`
	bodyfat=`echo $line | cut -f4 -d';'`

	echo "Sending ${date} - ${weight} "
	curl -s --location -X POST 'https://runalyze.com/api/v1/metrics/bodyComposition' \
		--header 'token: '${token} \
		-d "{ \
		      \"date_time\": \"${date}T${time}Z\", \
		      \"weight\": ${weight}, \
		      \"fat_percentage\": ${bodyfat} \
	           }"
	#don't overload :-)
	sleep 10
	echo

done < history.csv

