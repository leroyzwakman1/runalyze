# runalyze

Runalyze scripts

can have token secret `.runalyze.token`, fill this with `echo -n '<token' > .runalyze.token` to prevent newlines

## Add bloodpressure
I use an export from the app called Bloodpressure diary:  [Play store](https://play.google.com/store/apps/details?id=com.bluefish.bloodpressure ) 

### Example input
This export can be generated through google drive, or build / generate your own:
```
Sys,dia,pulse,epoch,???
114,70,59,1609447500898,3,
109,65,51,1609493520220,3,
```
*the last column is unknown, i assume the type / level.*

### Run script
setup your token as describe above
```
bash uploadBloodmetrics.sh
```
TODO: There should be some kind of counter to write what has been imported

### Add Weights:
Assuming you have the export data;
```
#fetch all 2020 records (i just only this...)
grep --no-group-separator -ri '2020-' -B1 ~/Downloads/endomondo-2020-12-28/Weights/10.json > 2lines.txt 

# https://stackoverflow.com/questions/9605232/how-to-merge-every-two-lines-into-one-from-the-command-line << thankx :) 
awk 'NR%2{printf "%s ",$0;next;}1' 2lines.txt > oneline.txt

#example output oneline.txt
#        {"date": "2020-03-29 06:00:00.0"}         {"date": "2020-03-30 06:00:00.0"}
#        {"date": "2020-03-31 06:00:00.0"}         {"date": "2020-04-01 06:00:00.0"}
#        {"date": "2020-04-02 06:00:00.0"}         {"date": "2020-04-03 06:00:00.0"}

#import to runalyzer
bash uploadBodyComposition.sh 
```

#
